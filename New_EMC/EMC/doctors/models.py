from django.db import models
from django.conf import settings

User = settings.AUTH_USER_MODEL


class Doctor(models.Model):
    '''Модель анкеты Доктора'''
    name = models.OneToOneField(User, verbose_name='ФИО',
                                on_delete=models.CASCADE)
    add_date = models.DateField(verbose_name='дата добавления анкеты',
                                auto_now_add=True,)
    specializations = models.CharField(
                                max_length=255,
                                verbose_name='Специализация',
                                blank=True,
                                default=False,
                                )
    professional_category = models.CharField(
                                max_length=255,
                                verbose_name='Категория',
                                blank=True,
                                default=False)
    training = models.CharField(
                                max_length=255,
                                verbose_name='Образование',
                                blank=True,
                                default=False)

    def __str__(self):
        return f'{self.name},{self.specializations}'
