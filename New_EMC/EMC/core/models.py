from django.db import models
from django.conf import settings

from doctors.models import Doctor

User = settings.AUTH_USER_MODEL


class Client(models.Model):
    '''Модель анкеты пациента'''
    name = models.OneToOneField(User,
                                verbose_name='ФИО',
                                on_delete=models.CASCADE)
    sex = models.CharField(verbose_name='пол',
                           max_length=15,
                           blank=True,
                           default=False)
    bloodtype = models.IntegerField(verbose_name='группа крови',
                                    blank=True)
    rezusfactor = models.CharField(max_length=2,
                                   verbose_name='резус фактор',
                                   blank=True)
    chronicaldiseases = models.CharField(max_length=255,
                                         verbose_name='хронические заболевания',
                                         blank=True)
    add_date = models.DateField(auto_now_add=True,
                                verbose_name='дата создания анкеты')

    def __str__(self):
        return f'{self.name}'


class Record(models.Model):
    '''Модель записи приема к врачу'''
    client = models.ManyToManyField(Client,
                                    verbose_name='Пациент',
                                    )
    doctor = models.ManyToManyField(Doctor,
                                    verbose_name='Врач')
    add_time = models.DateTimeField(auto_now=True,
                                    verbose_name='Дата создание записи')
    record_date = models.DateTimeField(blank=True,
                                       verbose_name='Дата и время приема')
    anamnesis = models.TextField(verbose_name='Анамнез',
                                 blank=True)
    diagnosis = models.TextField(verbose_name='Диагноз',
                                 blank=True)
    treatment = models.TextField(verbose_name='Назначенное лечение',
                                 blank=True)

    def get_datetime(self):
        return f'{self.record_date}'
