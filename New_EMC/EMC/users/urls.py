
from django.urls import path
from users.views import UserLogin, UserCreations, SuccesUrl

urlpatterns = [
path('', UserLogin.as_view(), name='login'),
path('registrations/', UserCreations.as_view(), name='registrations',),
path('success_registrations/', SuccesUrl.as_view(), name='succes_url')

]