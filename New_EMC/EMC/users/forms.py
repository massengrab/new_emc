from django import forms
from django.contrib.auth.forms import UserCreationForm, UserChangeForm
from django.contrib.auth import get_user_model
from django.contrib.auth.forms import AuthenticationForm
from .models import User


class CustomUserCreationForm(UserCreationForm):
    '''Класс - форма для создания пользователя'''
    username = forms.CharField(label='Логин', widget=forms.TextInput(
                                        attrs={'class': 'form-input'}))
    password1 = forms.CharField(label='Пароль', widget=forms.PasswordInput(
                                        attrs={'class':'form-input'}))
    password2 = forms.CharField(label='Повторите пароль', widget=forms.PasswordInput(
                                        attrs={'class': 'form-input'}))

    class Meta:
        model = get_user_model()
        fields = ['username', 'email', 'phone', 'first_name',
                  'last_name', 'password1', 'password2']
        labels = {
             'email': 'Email',
             'first_name': 'Имя',
             'last_name': 'Фамилия',
             'phone': 'Номер телефона',
        }
        widgets = {
            'email': forms.TextInput(attrs={'class': 'form-input'}),
            'first_name': forms.TextInput(attrs={'class': 'form-input'}),
            'last_name': forms.TextInput(attrs={'class': 'form-input'}),
            'phone': forms.TextInput(attrs={'class': 'form-input'})
        }


class LoginUser(AuthenticationForm):
    '''Класс - форма для авторизации '''

    username = forms.CharField(label='Логин',
                             widget=forms.TextInput(attrs={'class': 'form-input'}))
    password = forms.CharField(label='Пароль',
                            widget=forms.PasswordInput(attrs={'class': 'form-input'}))

    class Meta:
        model = User
        fields = ['username', 'password']











