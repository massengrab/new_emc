from django.db import models
from django.contrib.auth.models import AbstractUser

# Create your models here.


class User(AbstractUser):
    ''' Добавляются к существущей модели User новые атрибуты -
        Имя,Фамилия,Отчество, тип пользователя, метод для определния группы пользователей,
        поле для определения активации пользователя'''
    first_name = models.CharField(max_length=100, blank=True,
                                  verbose_name='Имя')
    last_name = models.CharField(max_length=100, blank=True,
                                 verbose_name='Фамилия')
    middle_name = models.CharField(max_length=100, blank=True,
                                   verbose_name='Отчество')
    email = models.EmailField(max_length=255, blank=True,
                              verbose_name='Email')
    phone = models.CharField(max_length=15, blank=True,
                             verbose_name='Номер телефона')

#Атрибуты для определения типа пользователя

    is_client = models.BooleanField(blank=True, default=False,
                                    verbose_name='Пациент')
    is_doctor = models.BooleanField(blank=True, default=False,
                                    verbose_name='Врач')
    is_regwork = models.BooleanField(blank=True, default=False,
                                    verbose_name='регистратура')
#Атрибут для активации пользователя
    is_added = models.BooleanField(blank=True, default=False,
                                   verbose_name='Активирован')

    def get_full_name(self):
        return f'{self.first_name} {self.last_name} {self.middle_name}'

    def __str__(self):
        return self.get_full_name()


