from django.contrib.auth import get_user_model
from django.contrib.auth.views import LoginView
from django.urls import reverse_lazy
from django.views.generic import CreateView, TemplateView
from .forms import LoginUser, CustomUserCreationForm
from .models import User


class UserLogin(LoginView):
    '''Класс-представление для авторизации'''
    form_class = LoginUser
    template_name = 'login.html'
    extra_context = {'title': 'авторизация'}



    def get_success_url(self):
        if self.form_class.Meta.model.is_added:
            if self.form_class.Meta.model.is_client:
                return reverse_lazy('client_home')
            elif self.form_class.Meta.model.is_doctor:
                return reverse_lazy('doctor_home')
            elif self.form_class.Meta.model.is_regwork:
                return reverse_lazy('regwork_home')
        else:
            return reverse_lazy('succes_url')




class UserCreations(CreateView):
    '''Класс-представление для регистрации'''
    form_class = CustomUserCreationForm
    template_name = 'registrations.html'
    success_url = reverse_lazy('succes_url')


class SuccesUrl(TemplateView):
    template_name = 'success_registration.html'
