from django.urls import path
from .views import Base, UserNonAdded

urlpatterns = [
path('base/',Base.as_view(), name='regworkbase'),
path('User_non_added/',UserNonAdded.as_view(), name='User_non_added')
]