from django.db import models
from django.conf import settings

User = settings.AUTH_USER_MODEL
# Create your models here.


class RegisterWorker(models.Model):
    '''Модель для анкеты работника регистратуры'''
    name = models.OneToOneField(User, verbose_name='ФИО сотрудника',
                                on_delete=models.CASCADE)
    add_date = models.DateField(verbose_name=' дата добавления анкеты',
                                auto_now_add=True)
    professional_category = models.CharField(
                                max_length=255,
                                verbose_name='Категория работника',
                                blank=True,
                                default=False)
    training = models.CharField(
                                max_length=255,
                                verbose_name='Образование работника',
                                blank=True,
                                default=False)

    def __str__(self):
        return f'{self.name}'
