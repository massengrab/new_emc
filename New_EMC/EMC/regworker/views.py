
from django.shortcuts import render
from django.views.generic import TemplateView, DetailView, ListView
from users.models import User


# Create your views here.
class Base(TemplateView):
    template_name = 'base.html'


class UserNonAdded(ListView):
    '''Класс представление который выводит
    спискок неактивированных пользователей'''
    queryset = User.objects.filter(is_added=False).order_by('last_name')
    template_name = 'UserProfileList.html'
    context_object_name = 'users'
